'use strict';

const Enigma = require("./enigma");
const eng = new Enigma('blahblah')

let encodeString = eng.encode("nom nom nom");

console.log(encodeString);

let decodeString = eng.decode(encodeString);

console.log(decodeString);
