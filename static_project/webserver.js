'use strict';

const http = require("http");
const url = require("url");
const fs = require("fs");
const path = require("path");

let mimes = {
    '.htm' : 'text/html',
    '.css' : 'text/css',
    '.js' : 'text/javascript',
    '.gif' : 'image/gif',
    '.jpg' : 'image/jpeg',
    '.png' : 'image/png'
};

function webserver(req, res){
    let baseUrl = url.parse(req.url);
    let filepath = __dirname + (baseUrl.pathname === '/' ? '/index.htm' : baseUrl.pathname);
    console.log(filepath);
    fs.access(filepath, fs.F_OK, error => {
        if(!error){
            fs.readFile(filepath, (error,content)=>{
                if(!error){
                    let contentType = mimes[path.extname(filepath)]
                    res.writeHead(200, {'Content-type' : contentType});
                    res.end(content,'utf-8');
                } else {
                    res.writeHead(500);
                    res.end('<h1> Cannot readfile :( </h1>');
                }
            })
        } else {
            res.writeHead(404);
            res.end('<h1> 404 NOT FOUND :( </h1>');
        }
    })
}

http.createServer(webserver).listen(process.env.PORT,() => {
    console.log("Server Running on Port: ", process.env.PORT)
})