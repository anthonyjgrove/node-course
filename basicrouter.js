'use strict';

const http = require("http");
const url = require("url");
const qs = require("querystring");

let routes =  {
    "GET" : {
        '/': (req, res) => {
            res.writeHead(200, {'Content-type' : 'text/html'});
            res.end('<h1> Hello Router! </h1>');
        },
        '/about': (req, res) => {
            res.writeHead(200, {'Content-type' : 'text/html'});
            res.end('<h1> About Page </h1>');
        },
        '/api/getinfo': (req, res) => {
            res.writeHead(200, {'Content-type' : 'text/html'});
            res.end(JSON.stringify(req.queryParams));
        }
    },
    "POST" : {
        '/api/login': (req, res) => {
            let body = '';
            req.on('data', data => {
                body += data;
                if(body.length > 2000000){
                    res.writeHead(413, {'Content-type' : 'text/html'});
                    res.end('<h1> File tooo bIG </h1>');
                    res.connection.destory();

                }
                console.log(body.length)
            });
            req.on('end', () => {
                let params = qs.parse(body);
                console.log("User Name: ", params['username'] );
                res.end();
            })
        }
    },
    "NA": (req, res) => {
        res.writeHead(404);
        res.end('<h1> 404 NOT FOUND :( </h1>');
    }
}

function router(req, res){
    let baseUrl = url.parse(req.url, true);
    let resolveRoute = routes[req.method][baseUrl.pathname];
    if( resolveRoute != undefined){
        req.queryParams = baseUrl.query;
        resolveRoute(req,res);
    } else {
        routes['NA'](req,res);
    }
}  

http.createServer(router).listen(process.env.PORT,() => {
    console.log("Server Running on Port: ", process.env.PORT)
})