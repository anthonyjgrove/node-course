'use strict';

const http = require("http");

http
    .createServer((req, res) => {
        res.writeHead(200, {'Content-type' : 'text/html'});
        res.end('<h1> Hello World </h1>');
    })
    .listen(process.env.PORT, () => console.log("Server Running on Port 3000"));